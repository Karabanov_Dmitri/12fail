import java.io.*;
import java.lang.String;

/**
 * Created by karab on 07.04.2017.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        File file1 = new File();
        File file2 = new File();
        int i = 0;
        String[] strings = new String[3];
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\1.txt"));
        BufferedWriter output = new BufferedWriter(new FileWriter("src\\output.txt"));
        BufferedWriter error = new BufferedWriter(new FileWriter("src\\error.txt"));
        String string = null;

        while ((string = bufferedReader.readLine()) != null) {
            if (bufferedReader == null) {
                System.out.println("Файл пуст");
                break;
            }
            i = 0;
            File.stringRacional(string);

            try {
                strings = string.split(" ");
                file1.racionalSplit(strings[0]);
                file2.racionalSplit(strings[2]);
            } catch (NumberFormatException e) {
                error.write(string + "   //ошибка  \n");
                System.out.println("ошибка в записи строки");
                break;
            }


            switch (strings[1])// знак из массива
            {
                case "+":
                    System.out.println(string + " = " + file1.summ(file2));
                    output.write(string + " = " + file1.summ(file2) + "\n");
                    break;
                case "-":
                    System.out.println(string + " = " + file1.subtraction(file2));
                    output.write(string + " = " + file1.subtraction(file2) + "\n");
                    break;
                case "*":
                    System.out.println(string + " = " + file1.multiplication(file2));
                    output.write(string + " = " + file1.multiplication(file2) + "\n");
                    break;
                case ":":
                    System.out.println(string + " = " + file1.division(file2));
                    output.write(string + " = " + file1.division(file2) + "\n");
                    break;
                default:
                    error.write(string + "   //ошибка  \n");
                    System.out.println("Неправильно введены данные");
                    break;
            }

        }
        bufferedReader.close();
        output.close();
        error.close();
    }
}




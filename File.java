import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class File {
    private int num;
    private int denum;

    public File(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    public File() {
        this(1, 1);
    }

    public File(int num) {
        this(num, 1);
    }

    public void racionalSplit(String string) {
        String[] array = new String[3];
        int i = 0;

        for (String splitstring : string.split("/")) {
            array[i] = splitstring;
            i++;
        }
        this.num = Integer.parseInt(array[0]);
        this.denum = Integer.parseInt(array[1]);

    }

    public File summ(File file2) {
        File file = new File();
        file.num = this.num * file2.denum + this.denum * file2.num;
        file.denum = this.denum * file2.denum;
        return file;
    }

    public File subtraction(File file2) {
        File file = new File();
        file.num = this.num * file2.denum - this.denum * file2.num;
        file.denum = this.denum * file2.denum;
        return file;
    }

    public File multiplication(File file2) {
        File racional = new File();
        racional.num = this.num * file2.num;
        racional.denum = this.denum * file2.denum;
        return racional;
    }

    public File division(File file2) {
        File racional = new File();
        racional.num = this.num * file2.denum;
        racional.denum = this.denum * file2.num;
        return racional;
    }

    static boolean stringRacional(String stringRacional) {
        Pattern pattern = Pattern.compile("[-]*[1-9][0-9]*[/]*[-]*[1-9][0-9]*\\s[+:*-]*\\s[-]*[1-9][0-9]*[/]*[-]*[1-9][0-9]");
        Matcher matcher = pattern.matcher(stringRacional);
        return matcher.find();
    }

    public File reduction() {
        File racional = new File();
        int i;
        for (i = 2; i < denum; i++) {
            if (this.num % i == 0 && this.denum % i == 0) {
                racional.num = this.num / i;
                racional.denum = this.denum / i;

            }
        }
        return racional;
    }


    @Override
    public String toString() {
        return num + "/" + denum;
    }
}

